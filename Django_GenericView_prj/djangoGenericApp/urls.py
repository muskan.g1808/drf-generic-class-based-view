from django.urls import path, include
from .views import GenericAPIView,\
    GenericAPIViewDetail,ArticleViewSet
from rest_framework.routers import DefaultRouter

# use default router for viewset and register router for ArticleViewSet
router = DefaultRouter()
router.register('article',ArticleViewSet, basename= 'article')

urlpatterns = [
    path('viewset/',include(router.urls)),
    path('viewset/<int:pk>/',include(router.urls)),
    # path('article/', article_list),
    path('article/', ArticleAPIView.as_view()),
    path('generic/article/', GenericAPIView.as_view()),
    # path('detail/<int:pk>/', article_details)
    path('detail/<int:id>/', ArticleDetails.as_view()),
    path('generic/detail/<int:id>/', GenericAPIViewDetail.as_view())

]
