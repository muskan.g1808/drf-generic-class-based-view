from django.apps import AppConfig


class DjangogenericappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'djangoGenericApp'
